<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/12 13:56
 */

namespace Calculation\Payment\ecpay;


class Pay
{
    private $config;

    public function __construct()
    {
        $this->config = new Config();
    }

    public function pay($data)
    {
        require_once dirname(__DIR__) . '/ecpay/ECPayAIO_PHP/AioSDK/sdk/ECPay.Payment.Integration.php';

        try {
            $obj = new \ECPay_AllInOne();

            // 设置同步与异步
            $notify_url = $this->config->NotifyUrl;
            $return_url = $this->config->ReturnUrl;

            //服務參數
            #$obj->ServiceURL = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5";  //服務位置
            $obj->ServiceURL = "https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5";  //正式环境服務位置
            $obj->HashKey = 'X2s6E3hGy7r4MbpT'; //測試用Hashkey，請自行帶入ECPay提供的HashKey
            $obj->HashIV = 'ixAibhRYN1q1KGen';  //測試用HashIV，請自行帶入ECPay提供的HashIV
            $obj->MerchantID = '3176563'; //測試用MerchantID，請自行帶入ECPay提供的MerchantID
            $obj->EncryptType = '1'; //CheckMacValue加密類型，請固定填入1，使用SHA256加密
            if (isset($data['payMethod'])) {
                if ($data['payMethod'] == "credit") {
                    $obj->Send['ChoosePayment'] = \ECPay_PaymentMethod::Credit;//付款方式:信用卡
                } elseif ($data['payMethod'] == "atm") {
                    $obj->Send['ChoosePayment'] = \ECPay_PaymentMethod::ATM;//付款方式:信用卡
                } elseif ($data['payMethod'] == "barcode") {
                    $obj->Send['ChoosePayment'] = \ECPay_PaymentMethod::BARCODE;//付款方式:信用卡
                }
            }
            //基本參數(請依系統規劃自行調整)
            $MerchantTradeNo = "Test" . time();
            #$obj->Send['ReturnURL'] = "http://www.ecpay.com.tw/receive.php" ; //付款完成通知回傳的網址
            $obj->Send['MerchantTradeNo'] = $data['order_id']; //訂單編號
            $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s'); //交易時間
            $obj->Send['TotalAmount'] = $data['goods_total_price']; //交易金額不可有小数点
            $obj->Send['TradeDesc'] = $data['body']; //交易描述
            // $obj->Send['ChoosePayment'] = \ECPay_PaymentMethod::WebATM;  //付款方式:全功能
            #$obj->Send['ClientBackURL'] = "http://www.baidu.com" ; //Client 端返回 特 店 的按鈕連結
            $obj->Send['ReturnURL'] = $notify_url; //付款完成通知回傳的網址 异步
            $obj->Send['OrderResultURL'] = $return_url; //同步 Client端回傳付款結果網址

            //訂單的商品資料
            array_push($obj->Send['Items'], array('Name' => $data['goods_name'], 'Price' => (int)$data['goods_total_price'],
                'Currency' => "元", 'Quantity' => (int)"1", 'URL' => "dedwed"));
            //產生訂單(auto submit至ECPay)
            $response = $obj->CheckOut();

            return $response;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     *  查询订单
     * @param int $order_id 订单id
     * @return bool
     */
    public function query($order_id)
    {
        require_once dirname(__DIR__) . '/ecpay/ECPayAIO_PHP/AioSDK/sdk/ECPay.Payment.Integration.php';

        $obj = new \ECPay_AllInOne();

        //服務參數
        #$obj->ServiceURL  = "https://payment-stage.ecpay.com.tw/Cashier/QueryTradeInfo/V5"; //服務位置
        $obj->ServiceURL = "https://payment.ecpay.com.tw/Cashier/QueryTradeInfo/V5"; //服務位置
        $obj->HashKey = $this->config->HashKey; //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV = $this->config->HashIV; //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID = $this->config->MerchantID; //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = '1'; //CheckMacValue加密類型，請固定填入1，使用SHA256加密

        //基本參數(請依系統規劃自行調整)
        $obj->Query['MerchantTradeNo'] = $order_id;
        $obj->Query['TimeStamp'] = time();

        //查詢訂單
        $info = $obj->QueryTradeInfo();
        if ($info['TradeStatus'] == "1") {
            return true;
            // echo "订单成立已付款"."<br>";
        }
        return false;
        //顯示訂單資訊
        // return $info;
    }

    /**
     * 读取配置项
     * @param $config
     */
    public function setOptions($config)
    {
        $this->config->HashKey = $config['HashKey'];
        $this->config->HashIV = $config['HashIV'];
        $this->config->MerchantID = $config['MerchantID'];
        $this->config->NotifyUrl = $config['NotifyUrl'];
        $this->config->ReturnUrl = $config['ReturnUrl'];
    }
}