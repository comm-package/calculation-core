<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/9 15:49
 */

namespace Calculation\Payment\paypal;


use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

class Pay
{
    private $config;

    public function __construct()
    {
        $this->config = new Config();
    }

    /**
     * payPal支付
     * @param $data
     * @return string|null|array
     */
    public function pay($data)
    {
        $PayPal_clientId = $this->config->clientId;
        $PayPal_clientSecret = $this->config->clientSecret;
        $mode = $this->config->mode;
        $httpProxy = $this->config->httpProxy;
        $httpConnectionTimeOut = $this->config->httpConnectionTimeOut;

        $paypal = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $PayPal_clientId, #id
                $PayPal_clientSecret #secret
            )
        );

        $configArr = ['mode' => $mode];
        if ($httpProxy) {
            $configArr['http.Proxy'] = $httpProxy;
            $configArr['http.ConnectionTimeOut'] = $httpConnectionTimeOut;
        }

        $paypal->setConfig($configArr);
        /*$paypal->setConfig(
            array(
                'mode' => 'live', //模式(live | sandbox) 开发or沙箱
                // 'mode' => 'sandbox', // live
                'http.ConnectionTimeOut' => 30,
                // 'http.Retry' => 1,
                'http.Proxy' => '47.243.118.133:8989',
            )
        );*/

        $product = $data['goods_name'];
        $price = $data['goods_total_price'];
        $shipping = 0.00; //运费
        $total = $price + $shipping;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName($product)
            ->setCurrency($data['currency'])
            ->setQuantity(1)
            ->setPrice($price);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setShipping($shipping)
            ->setSubtotal($price);
        #货币选择 https://developer.paypal.com/docs/subscriptions/reference/customize-the-sdk/?mark=currency#currency
        # HKD（港币） TWD （新台币）
        if (empty($data['currency'])) {
            $currency = "HKD";
        } else {
            $currency = $data['currency'];
        }
        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($total)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($data['body'])
            ->setInvoiceNumber($data['order_id']);#订单号

        $redirectUrls = new RedirectUrls();

        // 设置同步与异步
        $data['return_url'] = $this->config->returnUrl . "/order_id/" . $data['order_id'];
        $data['notify_url'] = $this->config->returnUrl;

        $redirectUrls->setReturnUrl($data['return_url']) #正向地址
        ->setCancelUrl("{$data['return_url']}?status=fail"); #取消支付地址

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $res = $payment->create($paypal);
        } catch (\Exception $e) {
            echo $e->getMessage();die();
        }
        $approvalUrl = $payment->getApprovalLink();

        return ['response' => $res, 'approvalUrl' => $approvalUrl];
    }

    /**
     * 查询订单
     * @param string $paymentId 支付id
     * @param string $PayerID 付款人id
     * @return mixed
     */
    public function query($paymentId, $PayerID)
    {
        $PayPal_clientId = $this->config->clientId;
        $PayPal_clientSecret = $this->config->clientSecret;
        $paypal = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $PayPal_clientId, #id
                $PayPal_clientSecret #secret
            )
        );

        $mode = $this->config->mode;
        $httpProxy = $this->config->httpProxy;
        $httpConnectionTimeOut = $this->config->httpConnectionTimeOut;
        $configArr = ['mode' => $mode];
        if ($httpProxy) {
            $configArr['http.Proxy'] = $httpProxy;
            $configArr['http.ConnectionTimeOut'] = $httpConnectionTimeOut;
        }
        $paypal->setConfig($configArr);
        /*$paypal->setConfig(
            array(
                'mode' => 'live',
                // 'mode' => 'sandbox',#SANDBOX
                'http.ConnectionTimeOut' => 30,
                // 'http.Retry' => 1,
                'http.Proxy' => '47.243.118.133:8989',
            )
        );*/

        $payment = Payment::get($paymentId, $paypal);

        $execute = new PaymentExecution();
        $execute->setPayerId($PayerID);

        $result = $payment->execute($execute, $paypal);
        $json = str_replace("'", '', $result);
        $json = json_decode($json, true);
        return $json;
    }

    /**
     * 读取配置项
     * @param $config
     */
    public function setOptions($config)
    {
        $this->config->clientId = $config['clientId'];
        $this->config->clientSecret = $config['clientSecret'];
        // $this->config->sandboxClientId = $config['sandboxClientId'];
        // $this->config->sandboxClientSecret = $config['sandboxClientSecret'];
        $this->config->mode = $config['mode'];
        $this->config->httpProxy = $config['httpProxy'];
        $this->config->httpConnectionTimeOut = $config['httpConnectionTimeOut'];
        $this->config->notifyUrl = $config['notifyUrl'];
        $this->config->returnUrl = $config['returnUrl'];
    }
}