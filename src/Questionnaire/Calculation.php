<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/7 16:18
 */

namespace Calculation\Questionnaire;

use Calculation\Questionnaire\Question as QuestionClass;

/**
 * Class 计算用例
 */
class Calculation
{
    /**
     * 计算问卷结果
     * @param array $askList 用户提交的答案
     * @param array $questionCountList 问卷题目信息
     * @param string $type adult/youth 成年/青年
     * @return array
     * @throws \ErrorException
     */
    public static function Compute($askList, $questionCountList, $type = 'adult')
    {
        // 计算4色、三大特质的分数
        if ($type == 'youth') {
            $userResult = IArea::finalityComputeConditionByYouth($askList, $questionCountList);
        } else {
            $userResult = IArea::finalityComputeCondition($askList, $questionCountList);
        }

        $userResult = $userResult['data'];
        // 计算四色区间
        $green = IArea::getRedBlueYellowGreenArea('green', $userResult);
        $blue = IArea::getRedBlueYellowGreenArea('blue', $userResult);
        $red = IArea::getRedBlueYellowGreenArea('red', $userResult);
        $yellow = IArea::getRedBlueYellowGreenArea('yellow', $userResult);
        // 饼状图百分比分值
        $sum = $green + $blue + $red + $yellow;
        $arr = [];
        $arr['greenPercent'] = QuestionClass::roundNumber($green / $sum);
        $arr['bluePercent'] = QuestionClass::roundNumber($blue / $sum);
        $arr['redPercent'] = QuestionClass::roundNumber($red / $sum);
        $arr['yellowPercent'] = QuestionClass::roundNumber($yellow / $sum);

        if (array_sum($arr) > 100 || array_sum($arr) < 100) {
            $arr = QuestionClass::staticString($arr);
        }
        // 计算特质区间
        $firm = IArea::getFirmAndExpressionArea('firm', $userResult);
        $expressions = IArea::getFirmAndExpressionArea('expression', $userResult);
        $flexible = IArea::getFirmAndExpressionArea('flexible', $userResult);
        // 最终的结果
        $lastResult = [
            'red' => $red,
            'blue' => $blue,
            'green' => $green,
            'yellow' => $yellow,
            'red_persent' => $arr['redPercent'],
            'blue_persent' => $arr['bluePercent'],
            'yellow_persent' => $arr['yellowPercent'],
            'green_persent' => $arr['greenPercent'],
            'firm' => $firm,
            'expression' => $expressions,
            'flexible' => $flexible,
        ];
        return [
            'question_result' => json_encode($lastResult),
            'select_result' => json_encode($askList),
            'red' => $userResult['red'],
            'blue' => $userResult['blue'],
            'green' => $userResult['green'],
            'yellow' => $userResult['yellow'],
            'expression' => $userResult['expression'],  //表达
            'firm' => $userResult['firm'], //坚定
            'flexible' => $userResult['flexible'], //变通
        ];
    }
}