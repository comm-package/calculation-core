<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/29 20:32
 */

// 加密示例 (php命令行执行encrypt.php 参数1：要加密的文件；参数2：输出的加密文件路径)
# php encrypt.php  demo.php /usr/data

if (function_exists('beast_encode_file')) {
    $input_file = $argv[1]; //要加密的文件
    $output_file = $argv[2]; //输出的加密文件路径
    beast_encode_file($input_file, $output_file);
    echo 'success';
} else {
    echo 'error';
}