<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/8 18:11
 */

namespace Calculation\Payment\wxpay;


class Config
{
    public $appId;
    public $appSecret;
    public $mchId;
    public $key;
    public $signType;
    public $sslCertPath;
    public $sslKeyPath;
    public $notifyUrl;
    public $gatewayUrl;
    public $publicAppId;
    public $appletsAppId;
}