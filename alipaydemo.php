<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/8 16:41
 */

require_once __DIR__ . '/vendor/autoload.php';

use Calculation\Payment\alipay\Pay;

$config = [
    // 应用证书路径
    'appCertPublicKey' => './src/Payment/alipay/alisdk/appCertPublicKey.crt',
    // 支付宝公钥证书路径
    'alipayCertPublicKey' => './src/Payment/alipay/alisdk/alipayCertPublicKey_RSA2.crt',
    // 支付宝根证书路径
    'alipayRootCert' => './src/Payment/alipay/alisdk/alipayRootCert.crt',
    // appId
    'appId' => '2021001102607803',
    // 签名类型
    'signType' => 'RSA2',
    // 私钥
    'rsaPrivateKey' => 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCKb9HjdRd2/3WRR+lQEYmjCjGfeejq619z7Rj0Pm0BBSkKHle3AkaO0NfN4MeGn8D92CZQmX0OeYPa4vhkxY7gRGIL+7IurVsVfiw5mg/sV9gZiNoZ2LXTIXda6h0fbtZ3L1xmd3660yJCSDyA+aAvwte+MyZK4vrq91JSoSRFkogmdnPzJgz0zvnbCk9urlnBT3g0S83scGSwmnZUMgsFkFGimvbN1prRarQWLqgPtWZb77bx2kE4KAYCjCcckRz4BorgmE/qeircuEIhk7fWwq9DXdxE2mgDTWrfaZIm63ryjCJWiQEx1C3IkJrS5OuvoyGH2txlOqH5OpWg0W1bAgMBAAECggEAZO+y8U1mlnUcm5j7HE4tNJsyIz0X/nEvX674Ct2ZTK0ehyooel9GYssTDiFeWWIRrQP0q7+91tfys53Zzl/dFNfHRUV3XWE4kCs2w6j5AzHpdnS50S6ZJB+6jqc0JL8D0FLP5EYsOLvItttEgiWADlLaSuuwd9mXacp6U5T6hV6FPxrPos0PdR1A9Y0dWClylLo5rg3C62vnvLy1aUiIqkxQnblBJWs8RREwG9OX/iNlx8ThECUUakmvS+hYIHta+ZSovvYOOpBnnxUgEEpQ39l4dW8nCu9kRi0/EioQXNju0uYTkpsmv5Hv6VnUGMlj+Fq1hRXrL/d4YRamRQKPQQKBgQDUCSegEB9pUAqQoDLCeL0sBGSyK441TnGvpdZiSRiA/YTwIhZZnfj6ZLghpFW2Pngepw82DanvE17wOTkZXpWwS15+nVSrRYyc3azr5Ty4XlEExRidFDZ/5G8aXKrVfrsoaj+1/8O1NfNHfSWuB+C3E+R2FYz3v7HZAJkiKdf8BwKBgQCnJAlb748BlH+LzW6Sdhk81ZQTx6PVFewW2BUHSC+EBBMfcc6jc5pp8tGTGoXRyr/aI/9bnphGD4mPDIe02NYeQgYcs5AIrNJIx+bIQYNZ73POrCzV9Wqxh9LB1aEbgs7tBUAsboWOs50ZPKooDcMehsAuYiuwlWenG/O+qeMXDQKBgCMZtN6RbLHBV05jxqNc22wK+iRog20dpEJAgMHaedz4muYprmt9dDb3L2gaTToTE013uFPpZSU3nrLDtkkV2E/L2kO5hceohsN9CuAuHaQjsOPKQO3X58G6Lg4+IuWeyhrBo/Rb96jq2nAOLoulsDR+bYaCwMhOioFy0ObP6HpHAoGAbAwRuMruSXkz0q5ksm2B9x+ib/743AEl88aQIXr6rH7t1b/+cw+CXMdJz6yvoWVODLiElTe6/F+C72SMPc6G7FyGEvvJOrJt5frEJ+SiAvuWg54uBcp2xHknN4LgeSZgmjxwtchkslFOV0guP2JWJl+50MJJuAd9UDvzn+e3E+kCgYEAk/XQPih09bHQ5EQ5YQnU7b4CHVNQWsqRDlhE9+L5iBU1os9D4Q6VmoTihj+a6EoaOryQZ0paGEJs81KH+QJnxQ2bmMODF0gRGJ52Sude+pJpsH87dY1KBVTLEnnh1ndqLC1ddHm+vb2f8wanhzBb3GXKABsL+VQHt5W93/yTIDs=',
    // 异步通知地址
    'notifyUrl' => 'api/v1/pays/ali_notify',
];

$order = [
    'body' => '测试购买商品',
    'goods_name' => '人教版小学数学二年级',
    'order_id' => rand(100000000000, 9999999999999),
    'goods_total_price' => 0.01,
];


$aliPay = new Pay();
// 写入配置
$aliPay->setOptions($config);

// 创建支付
/*$res = $aliPay->pay($order);
var_dump($res);die;*/

// 查询订单
$orderDetail = $aliPay->query('167015801580306');
var_dump($orderDetail);die;
