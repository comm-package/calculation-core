<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/7 14:44
 */

namespace Calculation\Questionnaire;


/**
 * Class 计算问卷调查的公共方法
 */
class Question
{
    /**
     * 蓝红黄计算方法
     * @param int $value 用户选择的分数值
     * @return mixed
     */
    public static function setBlueRedYellowCount($value = 1)
    {
        $arr = [-9, -3, -3, 0, 5, 11, 17];
        return $arr[$value - 1];
    }


    /**
     * 其他颜色分数调整
     * @param int $value 用户选择的分数值
     * @return mixed
     */
    public static function setOtherColorCount($value = 1)
    {
        $arr = [1, 2, 3, 0, -5, -6, -7];
        return $arr[$value - 1];
    }

    /**
     * 绿色计算方法
     * @param int $value 用户选择的分数值
     * @return mixed
     */
    public static function setGreenCount($value = 1)
    {
        $arr = [-9, -3, -3, 4, 10, 16, 22];
        return $arr[$value - 1];
    }

    /**
     * 坚定的分数值
     * @param int $value 用户选择的分数值
     * @return mixed
     */
    public static function setConfirmCount($value = 1)
    {
        $arr = [1, 1, 1, 1, 2, 5, 10];
        return $arr[$value - 1];
    }

    /**
     * 坚定的分数值 再去计算低分高分
     * @param int $value 总坚定分
     * @param int $low 低分个数
     * @param int $height 高分个数
     * @param array $firmArr 影响分数集
     * @return float|int|array
     */
    public static function setConfirmLowHeight($value = 1, $low = 0, $height = 0, $firmArr)
    {
        $lowArr = [1, 0.9, 0.85, 0.8, 0.75, 0.7];
        $heightArr = [1, 1.1, 1.15, 1.2, 1.25, 1.3];
        $log = '';
        //原始值
        $value1 = $value;
        $value = $value * $lowArr[$low];
        $log .= $value1 . '*' . $lowArr[$low] . '=' . $value . PHP_EOL;
        $value1 = $value;
        $value = $value * $heightArr[$height];
        $log .= $value1 . '*' . $heightArr[$height] . '=' . $value . PHP_EOL;
        $oneNum = in_array(1, $firmArr) ? array_count_values($firmArr)[1] : 0;
        $log .= json_encode($firmArr) . PHP_EOL;
        for ($i = 1; $i <= $oneNum; $i++) {
            $value1 = $value;
            $value = $value * 0.995;
            $log .= $value1 . '*' . '0.995=' . $value . PHP_EOL;
        }

        return ['count' => number_format($value, 2), 'log' => $log];
    }


    /**
     * 计算表达方法
     * @param int $value 答案相加 总答案
     * @param array $answer 答案数组
     * @return array
     */
    public static function setExpresstionCount($value = 0, $answer = [])
    {
        $str = ''; //计算内容
        $answer1 = [$answer[25], $answer[80]];
        $answer2 = [$answer[28], $answer[31], $answer[73]];
        //条件
        $rule1_1 = [[1, 1], [1, 2], [2, 1], [1, 3], [3, 1], [1, 4], [4, 1], [2, 2], [2, 3], [3, 2], [2, 4], [4, 2], [3, 3], [3, 4], [4, 3]];
        $condition1 = self::computeConditionTwo($answer1, $rule1_1);
        $rule2_1 = [[1, 1], [1, 2], [2, 1]];
        $rule2_2 = [[6, 6, 7], [6, 7, 6], [7, 6, 6], [6, 7, 7], [7, 6, 7], [7, 7, 6], [7, 7, 7]];
        $condition2 = !self::computeConditionTwo($answer1, $rule2_1) && !self::computeConditionThree($answer2, $rule2_2);
        $rule3_1 = [[1, 1], [1, 2], [2, 1], [1, 3], [3, 1], [1, 4], [4, 1], [2, 2], [2, 3], [3, 2], [2, 4], [4, 2], [3, 3], [3, 4], [4, 3]];
        $rule3_2 = [[6, 6, '*'], [6, '*', 6], ['*', 6, 6], [7, 7, '*'], [7, '*', 7], ['*', 7, 7], [6, 7, '*'], [6, '*', 7], ['*', 6, 7], [7, 6, '*'], [7, '*', 6], ['*', 7, 6]];
        $condition3 = self::computeConditionTwo($answer1, $rule3_1) && !self::computeConditionThree($answer2, $rule3_2);
        $rule4_1 = [[1, 3], [3, 1], [1, 4], [4, 1], [2, 2], [2, 3], [3, 2], [2, 4], [4, 2], [3, 3], [3, 4], [4, 3]];
        $rule4_2 = [[6, 6, 7], [6, 7, 6], [7, 6, 6], [6, 7, 7], [7, 6, 7], [7, 7, 6], [7, 7, 7]];
        $condition4 = self::computeConditionTwo($answer1, $rule4_1) && self::computeConditionThree($answer2, $rule4_2);
        $rule5_1 = [[1, 1], [1, 2], [2, 1]];
        $rule5_2 = [[6, 6, 7], [6, 7, 6], [7, 6, 6], [6, 7, 7], [7, 6, 7], [7, 7, 6], [7, 7, 7]];
        $condition5 = self::computeConditionTwo($answer1, $rule5_1) && self::computeConditionThree($answer2, $rule5_2);

        if ($condition1) {
            // 条件一
            $str = "条件一.计算规则1过程：{$value} - ({$answer[73]} + {$answer[28]}) / 2";
            $value = $value - ($answer[73] + $answer[28]) / 2;
            $str = $str . "= {$value}";
        } else if ($condition2) {
            $str = "条件二.计算规则3过程：{$value}";
            $str = $str . "= {$value}";
        } else if ($condition3) {
            $str = "条件三.计算规则2过程：({$value} - {$answer[93]}) * 1.5";
            $value = ($value - $answer[93]) * 1.5;
            $str = $str . "= {$value}";
        } else if ($condition4) {
            $str = "条件四.计算规则2过程：({$value} - {$answer[93]}) * 1.5";
            $value = ($value - $answer[93]) * 1.5;
            $str = $str . "= {$value}";
        } else if ($condition5) {
            $str = "条件五.计算规则1过程：{$value} - ({$answer[73]} + {$answer[28]}) / 2";
            $value = $value - ($answer[73] + $answer[28]) / 2;
            $str = $str . "= {$value}";
        } else {
            $str = "全部条件不符合：等于总分";
            $str = $str . "= {$value}";
        }

        return $result = [
            'str' => $str,
            'value' => number_format($value, 2),
        ];
    }

    /**
     * 两条规则计算
     * @param $answerArr
     * @param $ruleArr
     * @return bool
     */
    public static function computeConditionTwo($answerArr, $ruleArr)
    {
        $result = false;
        foreach ($ruleArr as $key => $value) {
            if ($answerArr[0] == $value[0] && $answerArr[1] == $value[1]) {
                $result = true;
                break;
            }

        }
        return $result;
    }

    /**
     * 三条规则计算
     * @param $answerArr
     * @param $ruleArr
     * @return bool
     */
    public static function computeConditionThree($answerArr, $ruleArr)
    {
        $result = false;
        foreach ($ruleArr as $key => $value) {
            if (in_array('*', $value)) {
                $answerArr_1 = $answerArr;
                $arbitrarilyKey = array_search('*', $value);
                unset($value[$arbitrarilyKey]);
                unset($answerArr_1[$arbitrarilyKey]);
                $value = array_values($value);
                $answerArr_1 = array_values($answerArr_1);

                if ($answerArr_1[0] == $value[0] && $answerArr_1[1] == $value[1]) {
                    $result = true;
                    break;
                }
            } else {
                if ($answerArr[0] == $value[0] && $answerArr[1] == $value[1] && $answerArr[2] == $value[2]) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 变通计算
     * @param int $value 总分
     * @param array $answer 影响分数
     * @param array $otherArr 其他分数
     * @return array
     */
    public static function setFlexibleCount($value = 1, $answer = [], $otherArr)
    {
        $str = '';
        if (self::flexibleCondition1($answer, [1, 2, 3, 4], 3)) {
            $str = "条件一.其他问题全部是2分";
            $value = count($otherArr) * 2 + array_sum($answer);
            $str .= '=' . $value . PHP_EOL;
        } else if (self::flexibleCondition1($answer, [6, 7], 3)) {
            $str = "条件二.总分加20";
            $value += 20;
            $str .= '=' . $value . PHP_EOL;
        } else if (self::flexibleCondition1($answer, [5], 2)) {
            $coefficient = self::flexibleCondition2($answer, $otherArr);
            $str = "条件三.总分（{$value}）* {$coefficient}";
            $value = $value * $coefficient;
            $str .= '=' . $value . PHP_EOL;
        }
        $result = [
            'str' => $str,
            'value' => number_format($value, 2),
        ];

        return $result;
    }

    /**
     * 变通计算规则
     * @param $answerArr
     * @param $ruleArr
     * @param $countNum
     * @return bool
     */
    public static function flexibleCondition1($answerArr, $ruleArr, $countNum)
    {
        $num = 0;
        $result = false;
        foreach ($answerArr as $value) {
            foreach ($ruleArr as $value1) {
                $value == $value1 && $num++;
            }
        }
        if ($num >= $countNum) {
            $result = true;
        }
        return $result;
    }

    /**
     * 变通计算规则3——a\b\c
     * @param $answerArr
     * @param $otherArr
     * @return mixed
     */
    public static function flexibleCondition2($answerArr, $otherArr)
    {
        $rule = [0 => 1, 1 => 0.9, 2 => 1.1, 3 => 0.9];
        $num1_4 = 0;
        $num6_7 = 0;
        foreach ($answerArr as $value) {
            if ($value > 5) {
                $num6_7++;
            } else {
                $num1_4++;
            }
        }
        foreach ($otherArr as $value) {
            if ($value > 5) {
                $num6_7++;
            } else {
                $num1_4++;
            }
        }
        if ($num1_4 > $num6_7) return $rule[1];
        if ($num1_4 < $num6_7) return $rule[2];
        if ($num1_4 = $num6_7) return $rule[3];
        return $rule[0];

    }

    public static function mergeOrderArr($arr1, $arr2)
    {
        // 检查数组的合法性
        if (!is_array($arr1) || !is_array($arr2)) {
            return false;
        }
        // 两个数组中有空数组的处理
        if (empty($arr1)) {
            return $arr2;
        }
        if (empty($arr2)) {
            return $arr1;
        }

        $result = array();

        while (count($arr1) > 0 && count($arr2) > 0) {
            // 判断当前的最小元素,追加到新的数组中
            if ($arr1[0] > $arr2[0]) {
                $result[] = array_shift($arr2);
            } else {
                $result[] = array_shift($arr1);
            }
        }

        // 将剩余元素追加到结果数组中
        if (count($arr1) > 0) {
            $result = array_merge($result, $arr1);
        }
        if (count($arr2) > 0) {
            $result = array_merge($result, $arr2);
        }

        return $result;
    }

    /**
     * 计算最终结果的百分比
     * @param $float
     * @return false|float
     */
    public static function roundNumber($float)
    {
        $float = $float * 1000;
        $float = round($float);
        $float = $float / 10;
        $float = round($float);
        return $float;
    }

    /**
     * 新的百分比 确保相加得到100
     * User: lmg
     * Date: 2022/4/24 15:38
     * @param $float
     * @param int $num 前几个数的相加 达到100 减去100差
     * @return false|float|int
     */
    public static function roundNumber_jws($float, $num = 0)
    {
        $float = $float * 10000;
        $float = round($float);
        $float = $float / 100;
        $float = round($float);
        if (($num + $float) > 100) {
            return $float - ($num + $float - 100);
        } else {
            return $float;
        }
    }

    /**
     * 统计question结果超过100的 重新计算百分比
     * @param array $arr 4种区间值
     * @return array
     */
    public static function staticString(array $arr)
    {
        foreach ($arr as $k => $v) {
            $arr[$k] = (int)$v;
        }
        $keys = array_count_values($arr);
        $count = array_sum($arr) - 100;
        #求出统计数目 最大次数
        $max = max($keys);

        #如果值等于4个 说明没有相同值 直接返回
        if (count($keys) == 4) {
            // return $keys;
            $arr['greenPercent'] -= $count;
            return $arr;
        } elseif (count($keys) == 3) {
            $key = array_search("1", $keys);
            $color = array_search($key, $arr);
            $arr[$color] -= $count;
            return $arr;
        } elseif (count($keys) == 2) {
            #如果是2对 一样的 就证明是102
            if ($max == 2) {
                return self::countArrayTosum($keys, $arr, "2");
            } elseif ($max == 3) {
                $key = array_search("1", $keys);
                $color = array_search($key, $arr);
                #如果总数 减去100 小于出现3次
                if ($count < 3 && $count > 1) {
                    $arr = self::countArrayTosum($keys, $arr, "3");
                    if (array_sum($arr) < 100) {
                        $key = array_search("1", $keys);
                        $arr[$color] = $arr[$color] + 100 - array_sum($arr);
                        return $arr;
                    }
                }
                if ($count < 3 && $count < 1) {
                    $key = array_search("1", $keys);
                    $arr[$color] = $arr[$color] + 100 - array_sum($arr);
                    return $arr;
                }
                $arr[$color] -= $count;
                return $arr;
            }
        } elseif (count($keys) == 1) {
            $num = ($count / 4);
            if ($num) {
                foreach ($arr as $k => $v) {
                    $arr[$k] -= $num;
                }
            }
            return $arr;
        }
    }

    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    public static function objectToArray($obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)self::objectToArray($v);
            }
        }
        return $obj;
    }

    public static function app_preference($data, $username, $font = 0)
    {
        $html = file_get_contents('static/app_preference.html');
        if ($font == 1) {
            $html = file_get_contents('static/app_preference2.html');
        }
        $html = str_replace("\n\t\t", "", $html);
        $html = str_replace("\r\n\t\t", "", $html);
        $html = str_replace('jiang_name', $username, $html);
        $html = str_replace('goutong', $data['expression'], $html);
        $html = str_replace('jianding', $data['firm'], $html);
        $html = str_replace('biantong', $data['flexible'], $html);

        $html = self::color_four($data, $html, $font);
        $html = self::color_six($data, $html, $font);
        $html = str_replace('font-size:10pt', "", $html);
        $html = str_replace('font-family:微軟正黑體;', "", $html);
        // $html = str_replace('</span>', "", $html);
        $html = self::color_enght($data, $html, $font);
        $html = explode("jiang_fen", $html);
        $new_arr = [];
        $new_arr[0] = [];
        $new_arr[0][0] = explode("paginate", $html[0])[0];
        $new_arr[0][1] = explode("paginate", $html[0])[1];
        $new_arr[1][0] = $html[1];
        $html[2] = self::replace_area($html[2], $font);

        $jiang_paragraph_fen = explode("jiang_paragraph_fen", $html[2]);
        $new_arr[2][0] = $jiang_paragraph_fen[0];
        $new_arr[2][1] = $jiang_paragraph_fen[1];

        $new_arr[3][0] = mb_substr($html[3], 0, mb_strlen($html[3]) / 2) . "</p>";
        $new_arr[3][1] = "<p>" . mb_substr($html[3], mb_strlen($html[3]) / 2);
        $new_arr[4][0] = $html[4];


        return $new_arr;
        // die;
    }

    public static function replace_area($html, $font)
    {
        if ($font != 1) {
            $html = str_replace('由于你的沟通特质是位于第', '<div class="section" style="margin-bottom: 25px;">由于你的<span style="color:purple;">沟通特质</span>是位于第', $html);
            $html = str_replace('由于你的坚定特质是位于第', '</div><div class="section" style="margin-bottom: 25px;">由于你的<span style="color:purple;">坚定特质</span>是位于第', $html);
            $html = str_replace('由于你的变通特质是位于第', '</div><div class="section" style="margin-bottom: 25px;">由于你的<span style="color:purple;">变通特质</span>是位于第', $html) . "</div>";
            return $html;
        } else {
            $html = str_replace('由於你的溝通特質是位於第', '<div class="section" style="margin-bottom: 25px;">由於你的<span style="color:purple;">溝通特質</span>是位於第', $html);
            $html = str_replace('由於你的堅定特質是位於第', '</div><div class="section" style="margin-bottom: 25px;">由於你的<span style="color:purple;">堅定特質</span>是位於第', $html);
            $html = str_replace('由於你的變通特質是位於第', '</div><div class="section" style="margin-bottom: 25px;">由於你的<span style="color:purple;">變通特質</span>是位於第', $html) . "</div>";
            return $html;
        }
    }

    /**
     * 拼接文字 4
     * @param array $arr 接收的数据
     * @param string $str 替换的值
     * @param int $font font为1代表繁体 否者为简体
     * @return [type]      [description]
     */
    public static function color_four($arr, $str, $font)
    {
        $head_msg = $font != 1 ? include('./static/Msg.php') : include('./static/MsgF.php');
        $content = $font != 1 ? include('./static/Msgs.php') : include('./static/MsgsF.php');
        $expression = $head_msg['expression'][self::area($arr['expression'])];
        if ($font == 1) {
            $str = str_replace('jiang_expression_four', $expression, $str);
            $firm = $head_msg['firm'][self::area($arr['firm'])];
            $str = str_replace('jiang_firm_four', $firm, $str);
            $flexible = $head_msg['flexible'][self::area($arr['flexible'])];
            $str = str_replace('jiang_flexible_four', $flexible, $str);
            $str = str_replace('jiang_test_four', $content['ten'][self::area($arr['expression']) . self::area($arr['firm']) . self::area($arr['flexible'])], $str);
        } else {
            $str = str_replace('jiang_expression_four', $expression, $str);
            $firm = $head_msg['firm'][self::area($arr['firm'])];
            $str = str_replace('jiang_firm_four', $firm, $str);
            $flexible = $head_msg['flexible'][self::area($arr['flexible'])];
            $str = str_replace('jiang_flexible_four', $flexible, $str);
            $str = str_replace('jiang_test_four', $content['ten'][self::area($arr['expression']) . self::area($arr['firm']) . self::area($arr['flexible'])], $str);
        }

        return $str;
    }

    /**
     * 区域 第一区或者第二区
     * @param string $str 传入的字符串
     * @return int [type]      [description]
     */
    public static function area($str)
    {
        //第一区域
        if ($str >= 0 && $str <= 33) {
            return 1;
        }
        //第二区域
        if ($str >= 34 && $str <= 66) {
            return 2;
        }
        //第三区域
        if ($str >= 67 && $str <= 100) {
            return 3;
        }

    }

    /**
     * 拼接文字6
     * @param array $arr 接收的数据
     * @param string $str 替换的值
     * @param int $font font为1代表繁体 否者为简体
     * @return string|string[]
     */
    public static function color_six($arr, $str, $font)
    {
        //获取数组
        $head_msg = $font != 1 ? include('./static/Msg.php') : include('./static/MsgF.php');
        //第几单元
        $num = 0;
        //数组键值拼接
        $color = '';
        $colorname = 0;
        if ($arr['red_persent'] >= 23) {
            $num += 1;
            $color .= 'red_';
            $colorname += $arr['red_persent'];
        }

        if ($arr['blue_persent'] >= 23) {
            $num += 1;
            $color .= 'blue_';
            $colorname += $arr['blue_persent'];
        }

        if ($arr['green_persent'] >= 23) {
            $num += 1;
            $color .= 'green_';
            $colorname += $arr['green_persent'];
        }

        if ($arr['yellow_persent'] >= 23) {
            $num += 1;
            $color .= 'yellow';
            $colorname += $arr['yellow_persent'];
        }
        //去除最后一个_
        if (substr($color, strlen($color) - 1) == '_') {
            $color = substr($color, 0, strlen($color) - 1);
        }

        #替换的数据
        $replace = str_replace('jiang_blue', $arr['blue_persent'], $head_msg[$num][$color]);
        $replace = str_replace('jiang_green', $arr['green_persent'], $replace);
        $replace = str_replace('jiang_yellow', $arr['yellow_persent'], $replace);
        $replace = str_replace('jiang_red', $arr['red_persent'], $replace);
        $color_name = array_filter(explode('_', $color));
        // dump($color_name);die;
        /*$colorname = [];
        foreach($color_name as $k=>$v) {
            $colorname[$arr[$v."_persent"]] = $v;
        }*/
        // $replace = str_replace('The population proportion', roundNumber($colorname/$num/100)."%", $replace);

        /* 暂时取消头部*/
        $str = str_replace('jiang_Thinkingtraits', '<span>' . $replace . '</span>', $str);

        return $str;
    }

    public static function color_seven($arr, $str, $font)
    {
        $content = $font != 1 ? include('./static/Msgs.php') : include('./static/MsgsF.php');
        //定义一个起点
        $num = 0;
        $string = [];
        //定义颜色
        $color = '';
        $msg = '';
        $msg1 = '';
        $msg2 = '';

        if ($arr['red_persent'] >= 23) {
            $string[] = $font != 1 ? '人际性' : '人際性';
            $color .= 'red_';
            $num += 1;
        }
        if ($arr['blue_persent'] >= 23) {
            $string[] = $font != 1 ? '分析性' : '分析性';
            $color .= 'blue_';
            $num += 1;
        }
        if ($arr['green_persent'] >= 23) {
            $string[] = $font != 1 ? '架构性' : '架構性';
            $color .= 'green_';
            $num += 1;
        }
        if ($arr['yellow_persent'] >= 23) {
            $color .= 'yellow_';
            $string[] = $font != 1 ? '概念性' : '概念性';
            $num += 1;
        }
        //去除最后一个_
        if (substr($color, strlen($color) - 1) == '_') {
            $color = substr($color, 0, strlen($color) - 1);
        }
        if ($num == 1) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = '<span style="color:black;font-size:10pt">' . $jiang_swph . '</span><span style="color:#ff0000;">' . $string[0] . '</span>';
        }

        if ($num == 4) {
            $msg = '<span style="color:black;">';
            $msg .= ($font != 1) ? '你有一个真正的多元思考者，这是很极为罕见的！ 你拥有' : '你有一個真正的多元思考者，這是很極為罕見的！你擁有 </span>';
            $msg .= ($font != 1) ? '<span style="color:#ff0000;">分析性 架构性 人际性 和概念性</span>' : '<span style="color:#ff0000;">分析性 架構性 人際性 和概念性</span>';
        }
        if ($num == 2) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = '<span style="color:black;">' . $jiang_swph . '</span><span style="color:#ff0000;">' . $string[0] . '</span>' . '和<span style="color:#ff0000;">' . $string[1] . '</span>';
        }
        if ($num == 3) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = '<span style="color:black;">' . $jiang_swph . '</span><span style="color:#ff0000;">' . $string[0] . '、' . $string[1] . '、' . $string[2] . '</span>';
        }
        $str = str_replace('jiang_swph', $msg, $str);
        if ($font != 1) {
            $str = str_replace('第一区 沟通特质，第一区 坚定特质，', self::area_han($arr['expression']) . '沟通特质，' . self::area_han($arr['firm']) . '坚定特质', $str);
            $str = str_replace('第一区变通特质', self::area_han($arr['flexible']) . '变通特质', $str);
        } else {
            $str = str_replace('第一區 溝通特質，第一區 堅定特質，', self::area_han($arr['expression'], $font) . '溝通特質，' . self::area_han($arr['firm'], $font) . '堅定特質', $str);
            $str = str_replace('第一區變通特質', self::area_han($arr['flexible'], $font) . '變通特質', $str);
        }


        #段落1
        $msg1 .= $content['two'][$num][$color];
        $msg1 .= $content['three'][$num][$color];
        $msg1 .= $content['four'][$num][$color];
        $msg1 .= $content['five'][$num][$color];
        $msg1 .= $content['six'][$num][$color];
        $msg1 .= $content['seven'][$num][$color];
        $msg1 .= $content['eight'][$num][$color];
        $msg1 .= $content['nine'][$num][$color];

        $str = str_replace('jiang_analysis', $msg1, $str);

        #段落2
        $msg2 = $content['ten'][self::area($arr['expression']) . self::area($arr['firm']) . self::area($arr['flexible'])];
        $str = str_replace('jiang_Behavioraltraits', $msg2, $str);

        #段落3 重新定义msg
        $msg = '';
        $msg .= $content['eleven'][$num][$color];
        //定义 行为特质 沟通 坚定 变通 随机获取
        $expression = array_rand($content['twelve']['expression'][self::area($arr['expression'])], 3);
        $firm = array_rand($content['twelve']['firm'][self::area($arr['firm'])], 3);
        $flexible = array_rand($content['twelve']['flexible'][self::area($arr['flexible'])], 3);

        foreach ($expression as $v) {
            $merge_arr[] = $content['twelve']['expression'][self::area($arr['expression'])][$v];
        }
        foreach ($firm as $v) {
            $merge_arr[] = $content['twelve']['firm'][self::area($arr['firm'])][$v];
        }
        foreach ($flexible as $v) {
            $merge_arr[] = $content['twelve']['flexible'][self::area($arr['flexible'])][$v];
        }
        $merge_arr = array_unique($merge_arr);

        $msg .= "你" . implode("", $merge_arr);
        if (substr($msg, -1, 3)) {
            $msg = mb_substr($msg, 0, mb_strlen($msg) - 1);
        }
        // $msg .= "你".str_replace("你","",implode("", $merge_arr));

        //如果是绿黄
        if ($color == "green_yellow") {

            $msg .= $font != 1 ? '有时候你想[按常规]做事情的慾望,可能会与创造和直觉上的需要不一致。' : '有時候你想[按常規]做事情的慾望,可能會與創造和直覺上的需要不一致。';
        }
        if ($font != 1) {
            $word_blue = ['就算会使人感到尴尬,你很可能也不害怕发问令人难以应对的问题的。', '就算会使人感到尴尬,你很可能也不害怕发问令人难以应对的问题的。 你展望未来的想法是专注于未来一至三年内可以达成的目标。'];
            $word_red = ['你可以建立强大的联盟,在团队建立和指导也是出色的。', '你可以建立强大的联盟,在团队建立和指导也是出色的。 当你需要完成目标时,他们对你都是支持的。'];
            $word_green = ['虽然你想得到其他人的意见,你可能在委派工作时是困难的,因为你不太确定可以信任其他人正确地完成工作,而有时候你想要[按常规]工作的想法可能会打击创意。', '你想加入其他人,但有时你可能在委派工作时是困难的,因为你不太确定可以信任其他人正确地完成工作,而有时候你想要[按常规]工作的想法可能会打击创意。', '你可能在委派工作时是困难的,因为你不太确定可以信任其他人正确地完成工作,而有时候你想要[按常规]工作的想法可能会打击创意。', '虽然你喜欢和人一起工作,但你可能在委派工作时是困难的,因为你不太确定可以信任其他人正确地完成工作,而有时候你想要[按常规]工作的想法可能会打击创意。'];
        } else {
            $word_blue = ['就算會使人感到尷尬,你很可能也不害怕發問令人難以應對的問題的。', '就算會使人感到尷尬,你很可能也不害怕發問令人難以應對的問題的。 你展望未來的想法是專註於未來一至三年內可以達成的目標。'];
            $word_red = ['你可以建立强大的聯盟，在團隊建立和指導也是出色的。', '你可以建立强大的聯盟，在團隊建立和指導也是出色的。當你需要完成目標時，他們對你都是支持的。'];
            $word_green = ['雖然你想得到其他人的意見，你可能在委派工作時是困難的，因為你不太確定可以信任其他人正確地完成工作，而有時候你想要[按常規]工作的想法可能會打擊創意。', '你想加入其他人，但有時你可能在委派工作時是困難的，因為你不太確定可以信任其他人正確地完成工作，而有時候你想要[按常規]工作的想法可能會打擊創意。', '你可能在委派工作時是困難的，因為你不太確定可以信任其他人正確地完成工作，而有時候你想要[按常規]工作的想法可能會打擊創意。', '雖然你喜歡和人一起工作，但你可能在委派工作時是困難的，因為你不太確定可以信任其他人正確地完成工作，而有時候你想要[按常規]工作的想法可能會打擊創意。'];
        }
        if ($color == 'blue') {
            $msg .= $word_blue[array_rand($word_blue)];
        }
        if ($color == 'red') {
            $msg .= $word_red[array_rand($word_red)];
        }

        $msg .= $content['thirteen'][$num][$color];
        if ($color == 'green') {
            $msg .= $word_green[array_rand($word_green)];
        }
        //如果字符中包含 绿色 随机出
        if (strpos($color, 'green')) {
            $rand = rand(0, 1);
            if ($rand) {
                $msg .= $font != 1 ? "你可能是迅速和任务导向的,在时间管理和按时完成工作方面也是出色的。" : "你可能是迅速和任務導向的，在時間管理和按時完成工作方面也是出色的。";
            }
        }
        //结果判断 第5段结束 加上第六段文字
        if (is_array($content['fourteen'][$num][$color])) {
            $msg .= self::pan($color, $arr, $content['fourteen'][$num][$color], $font);
        } else {
            $msg .= $content['fourteen'][$num][$color];
        }
        $str = str_replace('jiang_leader', $msg, $str);
        return $str;
    }

    /**
     * 手机app获取pdf
     * @param array $arr 接收的数据
     * @param string $str 替换的值
     * @param int $font font为1代表繁体 否者为简体
     * @return string
     */
    public static function color_enght($arr, $str, $font)
    {
        $content = $font != 1 ? include('./static/Msgs.php') : include('./static/MsgsF.php');
        //定义一个起点
        $num = 0;
        $string = [];
        //定义颜色
        $color = '';
        $msg = '';
        $msg1 = '';
        $msg2 = '';

        if ($arr['red_persent'] >= 23) {
            $string[] = $font != 1 ? '人际性' : '人際性';
            $color .= 'red_';
            $num += 1;
        }
        if ($arr['blue_persent'] >= 23) {
            $string[] = $font != 1 ? '分析性' : '分析性';
            $color .= 'blue_';
            $num += 1;
        }
        if ($arr['green_persent'] >= 23) {
            $string[] = $font != 1 ? '架构性' : '架構性';
            $color .= 'green_';
            $num += 1;
        }
        if ($arr['yellow_persent'] >= 23) {
            $color .= 'yellow_';
            $string[] = $font != 1 ? '概念性' : '概念性';
            $num += 1;
        }
        //去除最后一个_
        if (substr($color, strlen($color) - 1) == '_') {
            $color = substr($color, 0, strlen($color) - 1);
        }
        if ($num == 1) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = $jiang_swph . $string[0];
        }

        if ($num == 4) {
            // $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = $font != 1 ? '你有一个真正的多元思考者，这是很极为罕见的！ 你拥有' : '你有一個真正的多元思考者，這是很極為罕見的！你擁有' . $font != 1 ? '分析性 架构性 人际性 和概念性' : '分析性 架構性 人際性 和概念性';
        }
        if ($num == 2) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = $jiang_swph . $string[0] . '和' . $string[1];
        }
        if ($num == 3) {
            $jiang_swph = $font != 1 ? '你拥有' : '你擁有';
            $msg = $jiang_swph . $string[0] . '、' . $string[1] . '、' . $string[2];
        }
        $msg .= $font != 1 ? "思维偏好，加上" : "思維偏好，加上";
        if ($font != 1) {
            $msg .= self::area_han($arr['expression']) . '沟通特质，' . self::area_han($arr['firm']) . '坚定特质和' . self::area_han($arr['flexible']) . '变通特质。';
        } else {
            $msg .= self::area_han($arr['expression'], $font) . '溝通特質，' . self::area_han($arr['firm'], $font) . '堅定特質和' . self::area_han($arr['flexible'], $font) . '變通特質';
        }
        $str = str_replace("jiang_yongyou", $msg, $str);


        #段落2
        $msg2 = $content['ten'][self::area($arr['expression']) . self::area($arr['firm']) . self::area($arr['flexible'])];
        $str = str_replace('jiang_Behavioraltraits', $msg2, $str);

        $msg = '';
        //结果判断 第5段结束 加上第六段文字
        if (is_array($content['fourteen'][$num][$color])) {
            $msg .= self::pan($color, $arr, $content['fourteen'][$num][$color], $font);
        } else {
            $msg .= $content['fourteen'][$num][$color];
        }
        $str = str_replace('jiang_leader', $msg, $str);
        return $str;
    }

    /**
     * 区域 第一区或者第二区
     * @param string $str 传入特质  判断各特质属于多少区间 （汉文版本）
     * @param int $font 简繁体
     * @return string [type]      [description]
     */
    public static function area_han($str, $font = 0)
    {
        //第一区域
        if ($str >= 0 && $str <= 33) {
            return $font == 1 ? '第一區' : '第一区';
        }
        //第二区域
        if ($str >= 34 && $str <= 66) {
            return $font == 1 ? '第二區' : '第二区';
        }
        //第三区域
        if ($str >= 67 && $str <= 100) {
            return $font == 1 ? '第三區' : '第三区';
        }

    }

    /**
     * 结果判断
     * @param string $str 颜色拼接 比如 red_blue
     * @param array $arr 接收的数组
     * @param array $content msgs的数组数据
     * @param int $font 是否繁体
     * @return  string [description]
     */
    public static function pan($str, $arr, $content, $font)
    {
        //拿区间
        $firl = self::area($arr['firm']);
        $expression = self::area($arr['expression']);
        $flexible = self::area($arr['flexible']);
        //定义为0判断 几个区间
        $n = 0;
        //判断几个在二区
        if ($firl == 2) {
            $n += 1;
        }
        if ($expression == 2) {
            $n += 1;
        }
        if ($flexible == 2) {
            $n += 1;
        }
        if ($str == 'blue_yellow') {
            //不同的字符串状态
            $string = $font != 1 ? "记得你抽象思维的偏好可能令他人难于明白你对他们们有什么期望，" : "記得你抽象思維的偏好可能令他人難於明白你對他們們有什麼期望，";
            //判断2坚或者3
            if ($firl == 2 || $firl == 3) {
                return $string . $content[0];
            }

            if ($n == 2) {
                return $string . $content[1];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[1];
            }
            //沟通2
            if ($expression == 2) {
                return $string . $content[2];
            }
            //沟通3
            if ($expression == 3) {
                return $string . $content[3];
            }
        }
        //红绿
        if ($str == 'red_green') {
            //不同的字符串状态
            $string = $font != 1 ? "记得偏好理论性思维的人可能认为你的实务取向枯燥乏味，" : "記得偏好理論性思維的人可能認為你的實務取向枯燥乏味，";
            //沟通3
            if ($expression == 3) {
                return $string . $content[0];
            }
            if ($n == 2) {
                return $string . $content[1];
            }
        }
        //红黄
        if ($str == 'red_yellow') {
            //不同的字符串状态
            $string = $font != 1 ? "记得你『右脑』思维的偏好可能令他人难于明白你对他们有什么期望，" : "記得你『右腦』思維的偏好可能令他人難於明白你對他們有什麼期望，";
            //沟通3
            if ($expression == 3) {
                return $string . $content[2];
            }
            if ($n == 2) {
                return $string . $content[0];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[1];
            }
        }
        //单黄
        if ($str == 'yellow') {
            //不同的字符串状态
            $string = $font != 1 ? "记得你的概念性思维偏好可能令他人难于明白你对他们有什么期望，" : "記得你的概念性思維偏好可能令他人難於明白你對他們有什麼期望，";
            //判断2坚或者3
            if ($firl == 2 || $firl == 3) {
                return $string . $content[0];
            }
            if ($n == 2) {
                return $string . $content[1];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[2];
            }
            //沟通2
            if ($expression == 2) {
                return $string . $content[3];
            }
            //沟通3
            if ($expression == 3) {
                return $string . $content[4];
            }
        }
        //单蓝
        if ($str == 'blue') {
            //不同的字符串状态
            $string = $font != 1 ? "记得偏好分析性思维的人就算他们是很关心的，也可能面对被认为是没情感和不关心的风险，" : "记得偏好分析性思维的人就算他们是很关心的，也可能面对被认为是没情感和不关心的风险，";
            //判断2坚或者3
            if ($firl == 2 || $firl == 3) {
                return $string . $content[0];
            }
            if ($n == 2) {
                return $string . $content[1];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[2];
            }
            //沟通2
            if ($expression == 2) {
                return $string . $content[3];
            }
            //沟通3
            if ($expression == 3) {
                return $string . $content[4];
            }
        }
        //蓝绿
        if ($str == 'blue_green') {
            //不同的字符串状态
            $string = $font != 1 ? "记得运用很多『左脑』思维的人面对被认为令人紧张不安的风险，" : "記得運用很多『左腦』思維的人面對被認為令人緊張不安的風險，";
            //判断2坚或者3
            if ($firl == 2 || $firl == 3) {
                return $string . $content[0];
            }
            if ($n == 2) {
                return $string . $content[1];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[2];
            }
            //沟通2
            if ($expression == 2) {
                return $string . $content[3];
            }
            //沟通3
            if ($expression == 3) {
                return $string . $content[4];
            }
        }
        //单绿
        if ($str == 'green') {
            //不同的字符串状态
            $string = $font != 1 ? "记得结构性思维的人可能面对被认为是顽固的风险，" : "記得結構性思維的人可能面對被認為是頑固的風險，";
            //判断2坚或者3
            if ($firl == 2 || $firl == 3) {
                return $string . $content[0];
            }
            if ($n == 2) {
                return $string . $content[1];
            }
            //111 区间
            if ($firl == 1 && $expression == 1 && $flexible == 1) {
                return $string . $content[2];
            }
            //沟通2
            if ($expression == 2) {
                return $string . $content[3];
            }
            //沟通3
            if ($expression == 3) {
                return $string . $content[4];
            }
        }
    }

    /**
     * 计算该减去几次
     * @param $keys
     * @param $arr
     * @param $sum
     * @return mixed
     */
    public static function countArrayTosum($keys, $arr, $sum)
    {
        $key = array_search($sum, $keys);
        for ($i = 0; $i < $sum; $i++) {
            $color = array_search($key, $arr);
            $arr[$color] -= 1;
        }
        return $arr;
    }
}