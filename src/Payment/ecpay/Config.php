<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/12 13:56
 */

namespace Calculation\Payment\ecpay;


class Config
{
    public $MerchantID;
    public $HashIV;
    public $HashKey;
    public $ServiceURL;
    public $NotifyUrl;
    public $ReturnUrl;
}