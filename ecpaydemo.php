<?php
/**
 * Created by PhpStorm.
 * User: lmg
 * Date: 2022/12/12 14:15
 */

require_once __DIR__ . '/vendor/autoload.php';

use Calculation\Payment\ecpay\Pay;

$config = [
    // HashKey
    'HashKey' => 'X2s6E3hGy7r4MbpT',
    // HashIV
    'HashIV' => 'ixAibhRYN1q1KGen',
    // MerchantID
    'MerchantID' => '3176563',
    // 异步通知地址
    'NotifyUrl' => 'http://newcaixi.com/api/v1/ecnotify',
    // 同步通知地址
    'ReturnUrl' => 'http://newcaixi.com/api/v1/ec_return',
];

$order = [
    'body' => '测试购买商品',
    'goods_name' => '人教版小学数学二年级',
    'order_id' => rand(100000000000, 9999999999999),
    'goods_total_price' => 1,
    'payMethod' => 'credit', //付款方式：：当为ecpay的时候 信用卡（credit） ATM（atm） 超市条码（barcode）
];

$ECPay = new Pay();
$ECPay->setOptions($config);
// EC支付
// $res = $ECPay->pay($order);

/*$res = $ECPay->query('167055105517023');
var_dump($res);die;*/