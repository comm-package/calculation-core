# php-beast扩展加密

### 官方文档
    php-beast扩展加密
    github https://github.com/liexusong/php-beast

### 扩展安装
    编译安装如下:
        $ wget https://github.com/liexusong/php-beast/archive/master.zip
        $ unzip master.zip
        $ cd php-beast-master
        $ phpize
        $ ./configure
        $ make && make install
        编译好之后修改php.ini配置文件, 加入配置项: extension=beast.so, 重启php-fpm

### 代码加密
    如何加密代码(方案一)
        安装完 php-beast
        $ cd php-beast-master/tools
        $ vim configure.ini 文件
            src_path = ""  //是要加密项目的路径
            dst_path = ""  //是保存加密后项目的路径
            expire = ""  //是设置项目可使用的时间 (expire 的格式是：YYYY-mm-dd HH:ii:ss)
            encrypt_type = "DES"  //是加密的方式，选择项有：DES、AES、BASE64
    
            注：路径都是绝对路径 如 
                src_path = "/usr/local/ngninx/html/demo/"
    
        $ :wq
        $ php encode_files.php ##开始加密项目
    
    
    如何加密代码(方案二)
        使用beast_encode_file()函数加密文件，函数原型如下：
        beast_encode_file(string $input_file, string $output_file, int expire_timestamp, int encrypt_type)
    
        1. $input_file: 要加密的文件
        2. $output_file: 输出的加密文件路径
        3. $expire_timestamp: 文件过期时间戳
        4. $encrypt_type: 加密使用的算法（支持：BEAST_ENCRYPT_TYPE_DES、BEAST_ENCRYPT_TYPE_AES）

